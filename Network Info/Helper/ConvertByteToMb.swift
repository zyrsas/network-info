//
//  ConvertByteToMb.swift
//  Network Info
//
//  Created by sasha on 3/10/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation

class Convert {
    static func convertToMB(bytes: UInt32) -> String {
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let string = bcf.string(fromByteCount: Int64(bytes))
        return "\(string)"
    }
}
