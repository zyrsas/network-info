//
//  TestingController.swift
//  Network Info
//
//  Created by sasha on 3/13/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import UIKit

class TestingController: UIViewController, SimplePingDelegate, UITextFieldDelegate {
    
    
 
    @IBOutlet var btns: [radioButton]!
    @IBOutlet weak var startAndStopButton: UIButton!
    @IBOutlet weak var hostTextField: UITextField!
    @IBOutlet weak var logTextView: UITextView!
    
    var hostName = ""
    var pinger: SimplePing?
    var sendTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        hostTextField.delegate = self
        
        let gestureipv4 = UITapGestureRecognizer(target: self, action:  #selector(self.Ipv4_Pressed))
        self.btns[0].addGestureRecognizer(gestureipv4)
        
        let gestureipv6 = UITapGestureRecognizer(target: self, action:  #selector(self.Ipv6_Pressed))
        self.btns[1].addGestureRecognizer(gestureipv6)
        
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func Ipv4_Pressed(sender : UITapGestureRecognizer) {
        btns[0].radioSelected = true
        btns[1].radioSelected = false
    }
    
    @objc func Ipv6_Pressed(sender : UITapGestureRecognizer) {
        btns[0].radioSelected = false
        btns[1].radioSelected = true
    }
    
    @IBAction func BackButto_Pressed(_ sender: Any) {
        SetButtonStart()
        stop()
    }
    
 
   
    
    
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func SetButtonStart() {
        startAndStopButton.setTitle("Start",for: .normal)
        startAndStopButton.setTitleColor(UIColor.black, for: .normal)
    }
    
    func SetButtonStop() {
        startAndStopButton.setTitle("Stop",for: .normal)
        startAndStopButton.setTitleColor(UIColor.red, for: .normal)
    }
    
    func IsPing() -> Bool{
        if startAndStopButton.titleLabel?.text! == "Stop" {
            return true
        }
        return false
    }
    
    @IBAction func startButton_Pressed(_ sender: UIButton) {
        if !IsPing() {
            if (hostTextField.text == "") {
                return
            }
            view.endEditing(true)
            logTextView.text = ""
            SetButtonStop()
            hostName = hostTextField.text!
            self.start(forceIPv4: true, forceIPv6: false)
            return
        } else {
            SetButtonStart()
            stop()
        }
        
        
    }
    
    func start(forceIPv4: Bool, forceIPv6: Bool) {
        self.pingerWillStart()
        
        NSLog("start")
        
        let pinger = SimplePing(hostName: self.hostName)
        self.pinger = pinger
        
        // By default we use the first IP address we get back from host resolution (.Any)
        // but these flags let the user override that.
        
        if (forceIPv4 && !forceIPv6) {
            pinger.addressStyle = .icmPv4
        } else if (forceIPv6 && !forceIPv4) {
            pinger.addressStyle = .icmPv6
        }
        
        pinger.delegate = self
        pinger.start()
    }
    
    /// Called by the table view selection delegate callback to stop the ping.
    
    func stop() {
        NSLog("stop")
        self.pinger?.stop()
        self.pinger = nil
        
        self.sendTimer?.invalidate()
        self.sendTimer = nil
        
        self.pingerDidStop()
    }
    
    /// Sends a ping.
    ///
    /// Called to send a ping, both directly (as soon as the SimplePing object starts up) and
    /// via a timer (to continue sending pings periodically).
    
    @objc func sendPing() {
        self.pinger!.send(with: nil)
    }
    
    // MARK: pinger delegate callback
    
    func simplePing(_ pinger: SimplePing, didStartWithAddress address: Data) {
        NSLog("pinging %@", TestingController.displayAddressForAddress(address: address as NSData))
        logTextView.text.append("Pinging: \(TestingController.displayAddressForAddress(address: address as NSData)) \(NetworkType.networkType())\n")
        // Send the first ping straight away.
        
        let bottom = NSMakeRange(logTextView.text.count - 1, 1)
        logTextView.scrollRangeToVisible(bottom)
        
        self.sendPing()
        
        
        // And start a timer to send the subsequent pings.
        
        assert(self.sendTimer == nil)
        self.sendTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(TestingController.sendPing), userInfo: nil, repeats: true)
    }
    
    func simplePing(_ pinger: SimplePing, didFailWithError error: Error) {
        NSLog("failed: %@", TestingController.shortErrorFromError(error: error as NSError))
        logTextView.text.append("Failed: \(TestingController.shortErrorFromError(error: error as NSError))\n")
        
        self.stop()
        self.SetButtonStart()
        let bottom = NSMakeRange(logTextView.text.count - 1, 1)
        logTextView.scrollRangeToVisible(bottom)
    }
    
    var sentTime: TimeInterval = 0
    func simplePing(_ pinger: SimplePing, didSendPacket packet: Data, sequenceNumber: UInt16) {
        sentTime = Date().timeIntervalSince1970
        NSLog("#%u sent", sequenceNumber)
        logTextView.text.append("Sent: \(sequenceNumber). OK.\n")
        let bottom = NSMakeRange(logTextView.text.count - 1, 1)
        logTextView.scrollRangeToVisible(bottom)
       
    }
    
    func simplePing(_ pinger: SimplePing, didFailToSendPacket packet: Data, sequenceNumber: UInt16, error: Error) {
        NSLog("#%u send failed: %@", sequenceNumber, TestingController.shortErrorFromError(error: error as NSError))
        logTextView.text.append("Sending failed: \(TestingController.shortErrorFromError(error: error as NSError))\n")
        let bottom = NSMakeRange(logTextView.text.count - 1, 1)
        logTextView.scrollRangeToVisible(bottom)
        
    }
    
    func simplePing(_ pinger: SimplePing, didReceivePingResponsePacket packet: Data, sequenceNumber: UInt16) {
        let some = Int(((Date().timeIntervalSince1970 - sentTime).truncatingRemainder(dividingBy: 1)) * 1000)
        print("PING: \(some) MS")
        NSLog("#%u received, size=%zu", sequenceNumber, packet.count)
        logTextView.text.append("Received, size: \(packet.count); Time: \(some) MS. OK.\n")
        let bottom = NSMakeRange(logTextView.text.count - 1, 1)
        logTextView.scrollRangeToVisible(bottom)
    }
    
    func simplePing(_ pinger: SimplePing, didReceiveUnexpectedPacket packet: Data) {
        NSLog("unexpected packet, size=%zu", packet.count)
        logTextView.text.append("Unexpected packet\n")
        let bottom = NSMakeRange(logTextView.text.count - 1, 1)
        logTextView.scrollRangeToVisible(bottom)
    }
    
    // MARK: utilities
    
    /// Returns the string representation of the supplied address.
    ///
    /// - parameter address: Contains a `(struct sockaddr)` with the address to render.
    ///
    /// - returns: A string representation of that address.
    
    static func displayAddressForAddress(address: NSData) -> String {
        var hostStr = [Int8](repeating: 0, count: Int(NI_MAXHOST))
        
        let success = getnameinfo(
            address.bytes.assumingMemoryBound(to: sockaddr.self),
            socklen_t(address.length),
            &hostStr,
            socklen_t(hostStr.count),
            nil,
            0,
            NI_NUMERICHOST
            ) == 0
        let result: String
        if success {
            result = String(cString: hostStr)
        } else {
            result = "?"
        }
        return result
    }
    
    /// Returns a short error string for the supplied error.
    ///
    /// - parameter error: The error to render.
    ///
    /// - returns: A short string representing that error.
    
    static func shortErrorFromError(error: NSError) -> String {
        if error.domain == kCFErrorDomainCFNetwork as String && error.code == Int(CFNetworkErrors.cfHostErrorUnknown.rawValue) {
            if let failureObj = error.userInfo[kCFGetAddrInfoFailureKey as String] {
                if let failureNum = failureObj as? NSNumber {
                    if failureNum.intValue != 0 {
                        let f = gai_strerror(Int32(failureNum.intValue))
                        if f != nil {
                            return String(cString: f!)
                        }
                    }
                }
            }
        }
        if let result = error.localizedFailureReason {
            return result
        }
        return error.localizedDescription
    }
    
    
    func pingerWillStart() {
        
    }
    
    func pingerDidStop() {
      // SetButtonStart()
    }
       
}
