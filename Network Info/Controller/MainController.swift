//
//  ViewController.swift
//  Network Info
//
//  Created by sasha on 3/10/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import UIKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony
import Reachability
import JGProgressHUD


class MainController: UIViewController {
    
    @IBOutlet weak var TestButton: UIButton!
    @IBOutlet weak var defaultGatewayIP: UILabel!
    @IBOutlet weak var DNSServerIP: UILabel!
    @IBOutlet weak var DefaultGatewayIPv6: UILabel!
    @IBOutlet weak var DNSServerIPv6: UILabel!
    @IBOutlet weak var HTTPProxy: UILabel!
    // Wi-Fi informations
    @IBOutlet weak var WiFiNetworkConnected: UILabel!
    @IBOutlet weak var SSID: UILabel!
    @IBOutlet weak var BSSID: UILabel!
    @IBOutlet weak var Vendor: UILabel!
    @IBOutlet weak var IPAddress: UILabel!
    @IBOutlet weak var SubnetMask: UILabel!
    @IBOutlet weak var IPv6Address: UILabel!
    @IBOutlet weak var ResiveWiFI: UILabel!
    @IBOutlet weak var SentWiFi: UILabel!
    // Cell informations
    @IBOutlet weak var CellNetworkConnected: UILabel!
    @IBOutlet weak var NetworkType: UILabel!
    @IBOutlet weak var CellIPAddress: UILabel!
    @IBOutlet weak var CellIPv6Address: UILabel!
    @IBOutlet weak var CarrierName: UILabel!
    @IBOutlet weak var CountryCode: UILabel!
    @IBOutlet weak var MCC_MNC: UILabel!
    @IBOutlet weak var VOIPSupport: UILabel!
    @IBOutlet weak var ReceivedCell: UILabel!
    @IBOutlet weak var SentCell: UILabel!
    
    var proxyConfiguration = [NSObject: AnyObject]()
    var cellularInfo = CellularInfo()
    let reachability = Reachability()!
    var addresses = IPConfig()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TestButton.layer.borderWidth = 2
        TestButton.layer.borderColor = UIColor.black.cgColor
        
        self.refreshData()
        
  
    }
    
    
    func refreshData() {
   
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Loading"
      
        hud.show(in: self.view)
        
        
        self.defaultGatewayIP.text = DNS.getGatewayIP()
        self.DNSServerIP.text = DNS.getServers()
        self.DefaultGatewayIPv6.text = DNS.getGatewayIPv6()
        self.DNSServerIPv6.text = DNS.getDNSIPv6()
        self.HTTPProxy.text = ProxyInfo.getHttpProxyServer()
        // Wi-Fi informations
        self.WiFiNetworkConnected.text = WiFiInfo.getWiFiStatus()
        self.SSID.text = WiFiInfo.getWiFiSsid()
        self.BSSID.text = WiFiInfo.getWiFiBsid()
        
        if self.BSSID.text! == "N/A" {
            self.Vendor.text = "N/A"
        } else {
            VendorInfo.getVendorName(MAC: self.BSSID.text!) { (responce, error) in
                self.Vendor.text = responce!
                hud.dismiss(afterDelay: 0.2)
            }
        }
        
        self.IPAddress.text = Interface.getWiFiIpv4()
        self.SubnetMask.text = addresses.getWiFiMask()
        self.IPv6Address.text = Interface.getWiFiIpv6()
        self.ResiveWiFI.text = Convert.convertToMB(bytes: DataUsage.getDataUsage().wifiReceived)
        self.SentWiFi.text = Convert.convertToMB(bytes: DataUsage.getDataUsage().wifiSent)
        // Cell informations
        self.CellNetworkConnected.text = cellularInfo.getCellConected()
        self.NetworkType.text = cellularInfo.getNetworkType()
        self.CellIPAddress.text = Interface.getCellIpv4()
        self.CellIPv6Address.text = Interface.getCellIpv6()
        self.CarrierName.text = cellularInfo.getCelluarName()
        self.CountryCode.text = cellularInfo.getCountryCode()
        self.MCC_MNC.text = cellularInfo.getMCC_MNC()
        self.VOIPSupport.text = cellularInfo.getAllowsVOIP()
        self.ReceivedCell.text = Convert.convertToMB(bytes: DataUsage.getDataUsage().wirelessWanDataReceived)
        self.SentCell.text = Convert.convertToMB(bytes: DataUsage.getDataUsage().wirelessWanDataSent)
        hud.dismiss(afterDelay: 0.2)
    }
    
    @IBAction func RefreshButton_Pressed(_ sender: UIButton) {
        self.refreshData()
    }
    
    @IBAction func ConnectionCopy_Pressed(_ sender: UIButton) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Copied!"
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.show(in: self.view)
        
        UIPasteboard.general.string = """
        Gateway IP: \(defaultGatewayIP.text!)
        DNS Server IP: \(DNSServerIP.text!)
        Gateway IPv6: \(DNSServerIPv6.text!)
        DNS Server IPv6: \(DNSServerIPv6.text!)
        HTTP Proxy: \(HTTPProxy.text!)
        """
        
        hud.dismiss(afterDelay: 0.3)
    }
    
    @IBAction func WiFiConection_Pressed(_ sender: UIButton) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Copied!"
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.show(in: self.view)
        
        UIPasteboard.general.string = """
        Network Connected: \(WiFiNetworkConnected.text!)
        SSID: \(SSID.text!)
        BSSID: \(BSSID.text!)
        Vendor: \(Vendor.text!)
        IPv4 Address: \(IPAddress.text!)
        Subnet Mask: \(SubnetMask.text!)
        IPv6 Address: \(IPv6Address.text!)
        Received Since Boot: \(ResiveWiFI.text!)
        Sent Since Boot: \(SentWiFi.text!)
        """
        hud.dismiss(afterDelay: 0.3)
    }
    
    @IBAction func CellConnection_Pressed(_ sender: UIButton) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Copied!"
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.show(in: self.view)
        
        UIPasteboard.general.string = """
        Network Connected: \(CellNetworkConnected.text!)
        Network Type: \(NetworkType.text!)
        IPv4 Address: \(CellIPAddress.text!)
        IPv6 Address: \(CellIPv6Address.text!)
        Carrier Name: \(CarrierName.text!)
        Country Code: \(CountryCode.text!)
        MCC / MNC: \(MCC_MNC.text!)
        VOIP Support: \(VOIPSupport.text!)
        Received Since Boot: \(ReceivedCell.text!)
        Sent Since Boot: \(SentCell.text!)
        """
        
        hud.dismiss(afterDelay: 0.3)
    }
    
    @IBAction func AllCopy_Pressed(_ sender: UIButton) {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Copied!"
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.show(in: self.view)
        
        UIPasteboard.general.string = """
        Gateway IP: \(defaultGatewayIP.text!)
        DNS Server IP: \(DNSServerIP.text!)
        Gateway IPv6: \(DNSServerIPv6.text!)
        DNS Server IPv6: \(DNSServerIPv6.text!)
        HTTP Proxy: \(HTTPProxy.text!)
        Wi-Fi Information:
        Network Connected: \(WiFiNetworkConnected.text!)
        SSID: \(SSID.text!)
        BSSID: \(BSSID.text!)
        Vendor: \(Vendor.text!)
        IPv4 Address: \(IPAddress.text!)
        Subnet Mask: \(SubnetMask.text!)
        IPv6 Address: \(IPv6Address.text!)
        Received Since Boot: \(ResiveWiFI.text!)
        Sent Since Boot: \(SentWiFi.text!)
        Cellular Information:
        Network Connected: \(CellNetworkConnected.text!)
        Network Type: \(NetworkType.text!)
        IPv4 Address: \(CellIPAddress.text!)
        IPv6 Address: \(CellIPv6Address.text!)
        Carrier Name: \(CarrierName.text!)
        Country Code: \(CountryCode.text!)
        MCC / MNC: \(MCC_MNC.text!)
        VOIP Support: \(VOIPSupport.text!)
        Received Since Boot: \(ReceivedCell.text!)
        Sent Since Boot: \(SentCell.text!)
        """
        
        hud.dismiss(afterDelay: 0.3)
        
    }
    
    
}

