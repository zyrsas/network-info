//
//  CheckInternetType.swift
//  Network Info
//
//  Created by sasha on 3/13/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import Reachability


class NetworkType {
    
    static func networkType() -> String{
        var internetReachability = Reachability()
        if (internetReachability?.isReachable)! {
            if (internetReachability?.isReachableViaWiFi)! {
                return "via Wi-Fi. If you want to send via Cellular, turn off Wi-Fi"
            } else {
                return "via Cellular. If you want to send via Wi-Fi, turn on Wi-Fi"
            }
        } else {

            return "Not connected."
        }
    }
}
