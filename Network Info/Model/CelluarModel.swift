//
//  CelluarModel.swift
//  Network Info
//
//  Created by sasha on 3/11/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import CoreTelephony

class CellularInfo {
    let networkInfo = CTTelephonyNetworkInfo()
    
    func getCelluarName() -> String {
        let carrier = networkInfo.subscriberCellularProvider
        if (carrier?.carrierName! == nil) {
            return "N/A"
        }
        return (carrier?.carrierName!)!
    }
    
    func getAllowsVOIP() -> String {
        let carrier = networkInfo.subscriberCellularProvider
        
        if (carrier?.allowsVOIP == nil) {
            return "N/A"
        }
        
        if (carrier?.allowsVOIP)! {
            return "Yes"
        } else {
            return "No"
        }
    }
    
    func getMCC_MNC() -> String {
        let carrier = networkInfo.subscriberCellularProvider
        if (carrier?.mobileCountryCode! == nil) || (carrier?.mobileNetworkCode! == nil)  {
            return "N/A"
        }
        return "\(carrier!.mobileCountryCode!) / \(carrier!.mobileNetworkCode!)"
    }
    
    func getCountryCode() -> String {
        let carrier = networkInfo.subscriberCellularProvider
        
        if (carrier?.isoCountryCode! == nil) {
            return "N/A"
        }
        return carrier!.isoCountryCode!
    }
    
    func getNetworkType() -> String {
        
        let telefonyInfo = CTTelephonyNetworkInfo()
        if let radioAccessTechnology = telefonyInfo.currentRadioAccessTechnology{
            switch radioAccessTechnology{
            case CTRadioAccessTechnologyLTE: return "LTE"
            case CTRadioAccessTechnologyWCDMA: return "WCDMA"
            case CTRadioAccessTechnologyEdge: return "EDGE"
            case CTRadioAccessTechnologyCDMA1x: return "CDMA1x"
            case CTRadioAccessTechnologyGPRS: return "GPRS"
            case CTRadioAccessTechnologyeHRPD: return "HRPD"
            case CTRadioAccessTechnologyHSDPA: return "HSDPA"
            case CTRadioAccessTechnologyHSUPA: return "HSUPA"
            case CTRadioAccessTechnologyCDMAEVDORev0: return "CDMAEVDORev0"
            case CTRadioAccessTechnologyCDMAEVDORevA: return "CDMAEVDORevA"
            case CTRadioAccessTechnologyCDMAEVDORevB: return "CDMAEVDORevB"
            default: return "N/A"
            }
        }
        return "N/A"
    }
    
    func getCellConected() -> String {
        if (DNS.isHaveCell()) {
            return "Yes"
        } else {
            return "No"
        }
        return "No"
    }
    
}

