//
//  getgateway.h
//  Network Info
//
//  Created by sasha on 3/11/18.
//  Copyright © 2018 sasha. All rights reserved.
//

#ifndef getgateway_h
#define getgateway_h

#include <stdio.h>

int getdefaultgateway(in_addr_t * addr);

#endif /* getgateway_h */

