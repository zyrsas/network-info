//
//  VendorModel.swift
//  Network Info
//
//  Created by sasha on 3/13/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import Alamofire

class VendorInfo {
    static func getVendorName(MAC: String, completionHandler:@escaping (String?, Error?) -> Void){
        Alamofire.request("http://api.macvendors.com/\(MAC)", method: .get).responseString { (response) in
            switch response.result {
            case .success:
                if (response.result.value?.contains("errors"))! {
                    completionHandler("N/A", nil)
                    return
                }
                completionHandler(response.result.value, nil)
            case .failure(let _):
                completionHandler("N/A", nil)
            }
        }
    }
}
