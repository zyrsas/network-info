//
//  ProxyModel.swift
//  Network Info
//
//  Created by sasha on 3/12/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation

class ProxyInfo {
    
    static func getHttpProxyServer() -> String {
        if (DNS.proxyName() == nil) {
            return "N/A"
        }
        return DNS.proxyName()!
    }
}
