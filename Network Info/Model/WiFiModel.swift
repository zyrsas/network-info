//
//  WiFiModel.swift
//  Network Info
//
//  Created by sasha on 3/10/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import SystemConfiguration.CaptiveNetwork
import CoreTelephony
import Reachability


struct NetInfo {
    let ip: String
    let netmask: String
}


class WiFiInfo {
    static func getWiFiSsid() -> String? {
        var ssid: String?
        var bsid: String?
        var ip: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    bsid = interfaceInfo[kCNNetworkInfoKeyBSSID as String] as? String
                    ip = interfaceInfo[kCFNetworkProxiesHTTPEnable as String] as? String
                    break
                }
            }
        }
        if ssid == nil{
            return "N/A"
        }
        return ssid
    }
    
    
    static func getWiFiBsid() -> String? {
        var bsid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    bsid = interfaceInfo[kCNNetworkInfoKeyBSSID as String] as? String
                    break
                }
            }
        }
        if bsid == nil{
            return "N/A"
        }
        return bsid
    }
    
    static func getProxy() -> String? {
        var bsid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    bsid = "N/A"
                    break
                }
            }
        }
        if bsid == nil{
            return "N/A"
        }
        return "\(bsid)"
    }
    
    
    
    
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var addr = interface.ifa_addr.pointee
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(&addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
    static func getWiFiStatus() -> String {
        let reachability = Reachability()!
        if reachability.connection == .wifi {
            return "Yes"
        }
        return "No"
    }
    
    static func getWiFiOn() -> Bool {
        let reachability = Reachability()!
        if reachability.connection == .wifi {
            return true
        }
        return false
    }
   
    
}
