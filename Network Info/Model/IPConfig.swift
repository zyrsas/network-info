//
//  IPConfig.swift
//  Network Info
//
//  Created by sasha on 3/12/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import Darwin

class IPConfig {
    var addresses = [NetInfo]()
    
    func getWiFiIP() -> String {
        addresses = getIFAddresses()
        if !WiFiInfo.getWiFiOn() {
            return "N/A"
        }
        if DNS.isHaveCell() {
            return addresses[1].ip
        }
        return addresses[0].ip
    }
    
    func getWiFiMask() -> String {
        addresses = getIFAddresses()
        if !WiFiInfo.getWiFiOn() {
            return "N/A"
        }
        if DNS.isHaveCell() {
            return addresses[1].netmask
        }
        return addresses[0].netmask
    }
    
    func getCellIP() -> String {
        if DNS.isHaveCell() {
            return addresses[0].ip
        }
        return "N/A"
    }
    
    // Get the ip addresses used by this node
    func getIFAddresses() -> [NetInfo] {
        var addresses = [NetInfo]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            
            var ptr = ifaddr;
            while ptr != nil {
                
                let flags = Int32((ptr?.pointee.ifa_flags)!)
                var addr = ptr?.pointee.ifa_addr.pointee
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                    if addr?.sa_family == UInt8(AF_INET) || addr?.sa_family == UInt8(AF_INET6) {
                        if (addr?.sa_family ==  UInt8(AF_INET6)) {
                           // print("IPV6")
                        }
                        if addr?.sa_family == UInt8(AF_INET) {
                          //  print("IPv4")
                        }
                        
                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        if (getnameinfo(&addr!, socklen_t((addr?.sa_len)!), &hostname, socklen_t(hostname.count),
                                        nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                            
                            
                            if let address = String.init(validatingUTF8:hostname) {
                               // print("address: \(address)")
                                var net = ptr?.pointee.ifa_netmask.pointee
                                var netmaskName = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                                getnameinfo(&net!, socklen_t((net?.sa_len)!), &netmaskName, socklen_t(netmaskName.count),
                                            nil, socklen_t(0), NI_NUMERICHOST)// == 0
                                if let netmask = String.init(validatingUTF8:netmaskName) {
                                    addresses.append(NetInfo(ip: address, netmask: netmask))
                                }
                            }
                        }
                    }
                }
                ptr = ptr?.pointee.ifa_next
            }
            freeifaddrs(ifaddr)
        }
        return addresses
    }
}
