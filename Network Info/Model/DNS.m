//
//  DNS.m
//  Network Info
//
//  Created by sasha on 3/11/18.
//  Copyright © 2018 sasha. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <resolv.h>
#include <dns.h>
#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <string.h>
#import "getgateway.h"


@interface DNS:NSObject

+ (NSString *) getDNSServers;
+ (NSString *)getAwayAddress;
+ (NSString *)getGatewayIP;
+ (BOOL)isProxyConnected;
+ (BOOL)isHaveCell;
+(NSString *)proxyName;
+ (NSString *)getDNSIPv6;
+ (NSString *)getGatewayIPv6;
@end

@implementation DNS

+ (NSString *) getDNSServers
{
    // dont forget to link libresolv.lib
    NSMutableString *addresses = [[NSMutableString alloc]initWithString:@"DNS Addresses \n"];
    
    res_state res = malloc(sizeof(struct __res_state));
    
    int result = res_ninit(res);
    
    if ( result == 0 )
    {
        NSLog(@"%d",res->nscount);
        for ( int i = 0; i < res->nscount; i++ )
        {
            NSString *s = [NSString stringWithUTF8String :  inet_ntoa(res->nsaddr_list[i].sin_addr)];
            return s;
            [addresses appendFormat:@"%@\n",s];
            NSLog(@"%@",s);
        }
    }
    else
        [addresses appendString:@" res_init result != 0"];
    
    return addresses;
}


+ (NSString *)getDNSIPv6 {
    res_state res = malloc(sizeof(struct __res_state));
    int result = res_ninit(res);
    if (result == 0) {
        union res_9_sockaddr_union *addr_union = malloc(res->nscount * sizeof(union res_9_sockaddr_union));
        res_getservers(res, addr_union, res->nscount);
        
        for (int i = 0; i < res->nscount; i++) {
            if (addr_union[i].sin.sin_family == AF_INET6) {
                char ip[INET6_ADDRSTRLEN];
                inet_ntop(AF_INET6, &(addr_union[i].sin6.sin6_addr), ip, INET6_ADDRSTRLEN);
                NSString *dnsIP = [NSString stringWithUTF8String:ip];
                NSLog(@"IPv6 DNS IP: %@", dnsIP);
                return dnsIP;
            }
        }
    }
    res_nclose(res);
    return @"N/A";
}


+ (NSString *)getAwayAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
                {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            if(temp_addr->ifa_addr->sa_family == AF_INET6)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
                {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
}

+ (NSString *)getGatewayIP
{
    NSString *ipString = @"";
    struct in_addr gatewayaddr;
    int r = getdefaultgateway(&(gatewayaddr.s_addr));
    if(r >= 0)
    {
        ipString = [NSString stringWithFormat: @"%s",inet_ntoa(gatewayaddr)];
    }
    if ([ipString isEqualToString:@""]) {
        return @"N/A";
    }
    
    return ipString;
}

+ (NSString *)getGatewayIPv6
{
    NSString *ipString = @"";
    struct in_addr gatewayaddr;
    int r = getdefaultgateway(&(gatewayaddr.s_addr));
    if(r >= 0)
    {
        ipString = [NSString stringWithFormat: @"%s",inet_ntoa(gatewayaddr)];
    }
    if ([ipString isEqualToString:@""]) {
        return @"N/A";
    }

    if ([ipString containsString:@":"]) {
        return ipString;
    }
    
    return @"N/A";
}


+ (BOOL)isHaveCell{
    
    struct ifaddrs* interfaces = NULL;
    
    struct ifaddrs* temp_addr = NULL;
    
    // retrieve the current interfaces - returns 0 on success
    NSInteger success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL)
        {
            
            NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
            
            
            if ([name isEqualToString:@"pdp_ip0"] && temp_addr->ifa_addr->sa_family ==2 ) {
                
                return TRUE;
                
            }
            
            temp_addr = temp_addr->ifa_next;
            
        }
    }

    // Free memory
    freeifaddrs(interfaces);
    
    return FALSE;
    
}

+ (BOOL)isProxyConnected
{
    NSDictionary *dict = CFBridgingRelease(CFNetworkCopySystemProxySettings());
    NSArray *keys = [dict[@"__SCOPED__"]allKeys];
    for (NSString *key in keys) {
        if ([key rangeOfString:@"tap"].location != NSNotFound ||
            [key rangeOfString:@"tun"].location != NSNotFound ||
            [key rangeOfString:@"ipsec"].location != NSNotFound ||
            [key rangeOfString:@"ppp"].location != NSNotFound){
            return YES;;
        }
    }
    return NO;
}

+(NSString *)proxyName
{
    
    CFDictionaryRef dicRef = CFNetworkCopySystemProxySettings();
    
    const CFStringRef proxyCFstr = (const CFStringRef)CFDictionaryGetValue(dicRef,
                                                                           (const void*)kCFNetworkProxiesHTTPProxy);
    
    return  (__bridge NSString *)proxyCFstr;
    
}

@end


